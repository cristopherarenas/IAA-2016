#
# Este archivo debe llamarse nombre_algoritmovParamILS.sh
#
#!/bin/bash

#bench=$1
seed=$5

#input_file = ARGV[0]
#instance_specifics = ARGV[1]
#timeout = ARGV[2].to_i
#cutoff_length = ARGV[3].to_i
#seed = ARGV[4].to_i

shift 5

while [ $# != 0 ]; do
    flag="$1"
    case "$flag" in
        -localsearch) if [ $# -gt 1 ]; then
              arg="$2"
              shift
              #AQUI IR CAMBIANDO POR LOS PARAMETROS DEFINIDOS EN alg_ant.params en orden!
              localsearch=$arg
            fi
            ;;
        -alpha) if [ $# -gt 1 ]; then
              arg="$2"
              shift
              alpha=$arg
            fi
            ;;
        -beta) if [ $# -gt 1 ]; then
              arg="$2"
              shift
              beta=$arg
            fi
            ;;
        -rho) if [ $# -gt 1 ]; then
              arg="$2"
              shift
              rho=$arg
            fi
            ;;
        -ant) if [ $# -gt 1 ]; then
              arg="$2"
              shift
              ant=$arg
            fi
            ;;
        -feromona_ini) if [ $# -gt 1 ]; then
              arg="$2"
              shift
              feromona_ini=$arg
            fi
            ;;           
        -iter) if [ $# -gt 1 ]; then
              arg="$2"
              shift
              iter=$arg
            fi
            ;;
        *) echo "Unrecognized flag or argument: $flag"
            ;;
        esac
    shift
done

#tries=1
#tours=100 -s ${tours}
#nnants=10 -g ${nnants}
#ttime=5
start=`date +%s`
outfile=data/data_${start}.dat
#crea carpeta data/ donde almacena los outputs de todas las corridas que ejecute ParamsILS (a modo de respaldo)
echo "./alg_ant -s ${seed}  -n ${iter} -h ${ant} -a ${alpha} -b ${beta} -r ${rho} -f ${feromona_ini} -l ${localsearch}> ${outfile}"
./alg_ant -s ${seed}  -n ${iter} -h ${ant} -a ${alpha} -b ${beta} -r ${rho} -f ${feromona_ini} -l ${localsearch} > ${outfile}
end=`date +%s`

#Calcular hipervolumen del frente
rm -f result.txt
#el punto debe ser mayor a todos los componentes del frente de Pareto
./wfg ./Pareto.txt 10 30 10 50 80 300 10 10 10 >> result.txt


exec<"result.txt"

while read line
do
    set -- $line
    name=$1
    if [[ $name == "hv(1)" ]]; 
    then
        foo=$3
	res="$( cut -d '.' -f 1 <<< "$foo" )"
	let "res = res / 100000"
        let "res = 36000000 - res"
        echo "nombre: ${name}, resultado: ${res}"
    fi
done
#En la variable res almacena la diferencia de [36000000 - (hipervolumen obtenido)], el valor 36000000 es un valor muy grande,
#ya que ParamILS busca minimizar lo que tu entregas en res, por eso lo pusimos asi, la idea es que en vez de 36000000 pongas
# un valor que supere por harto los valores que hayas obtenido en tus hipervolumenes, o dejalo asi si este numero te sirve
solved="SAT"
runtime=$((end-start))

#echo "variable1 (from data-file) = $runlength"
#echo "variable3 (from data-file) = $runtime"

echo "Result for ParamILS: ${solved}, ${runtime}, ${res}, ${res}, ${seed}"
