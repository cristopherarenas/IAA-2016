#include "inmune.h"

using namespace std;

Inmune::Inmune(){
	
}

Inmune::Inmune(Problem &p,Opciones &o,vector<RouteInfo> &routes_info,ShortestRoute &sr){
		this->pop_size = o.get_popsize();
		this->clon_size = o.get_clonsize();
		this->generaciones = o.get_generaciones();
		this->prob_mutacion = o.get_probmutacion();
		this->pop_bc = 0;
		
		this->alpha = o.get_alpha(); 
		this->beta = o.get_beta();
		this->p_afinidad_clones = o.get_afinidad();
		this->p_clones = o.get_porcentajeclones();
		this->p_reemplazo = o.get_porcentajereemplazo();
		
		if(this->alpha > 1){
			cambiar_alpha_beta();
		}
		
		//obtencion de conjuto de soluciones factible
		this->ss = SolutionSet(p, routes_info,this->pop_size,sr,this->alpha,this->beta);
		int** demand = p.get_demand();
		int** travel = p.get_travel_times();
		int size = p.get_size();
		
		if(this->alpha>1){
			int random_number = 100*rand()%11;
			if(random_number<0){
				random_number = random_number*-1;
			}
			int a = random_number*10;
			int b = 100-random_number*10;
			this->alpha = a;
			this->beta = b;
			cout << "iniciando con alpha=" << a << " beta=" << b << endl;
		}
		/*
		//calculo de fo
		for(int i=0;i<this->ss.solutions.size();i++){
			this->ss.solutions[i].setFO1(sr,demand);
			this->ss.solutions[i].setFO2(size,travel);
			this->ss.solutions[i].setQuality(this->alpha,this->beta);
			//cout << "fo2 " << this->ss.solutions[i].fo2 << " fo1 " << this->ss.solutions[i].fo1 << " apt " << this->ss.solutions[i].quality << endl; 
		}*/
		int clon_size = o.get_clonsize();
		
		this->memory = SolutionSet(clon_size);
};

Inmune::~Inmune(){

};

void Inmune::print_solutions(){
	for(int i=0;i<this->ss.solutions.size();i++){
		cout << "r " << ss.solutions[i].ranking_pareto << ": fo2 " << this->ss.solutions[i].fo2 << " fo1 " << this->ss.solutions[i].fo1 << " apt " << this->ss.solutions[i].quality << endl; 
	}
}


void Inmune::print_memory(){
	for(int i=0;i<this->memory.solutions.size();i++){
		cout << "r " << memory.solutions[i].ranking_pareto << ": fo2 " << this->memory.solutions[i].fo2 << " fo1 " << this->memory.solutions[i].fo1 << " apt " << this->memory.solutions[i].quality << endl; 
	}
}

void Inmune::print_solutions(vector<Solution> &s){
	for(int i=0;i<s.size();i++){
		cout << "r " << s[i].ranking_pareto << ": fo2 " << s[i].fo2 << " fo1 " << s[i].fo1 << " apt " << s[i].quality << endl; 
	}
}
/*
void Inmune::eliminar_dominados(set<Solution> &poblacion){
	for(set<Solution>::iterator it=poblacion.begin(); it!=poblacion.end(); ++it){
		//revisar si son dominados con respecto a otras soluciones

	}
}
*/
void Inmune::eliminar_dominados(vector<Solution> &poblacion)
{
	int size = poblacion.size();
	//revisar si son dominados con respecto a otras soluciones
	for(int i=0;i<poblacion.size();i++)
	{
		poblacion[i].set_dominated(poblacion);
	}
	
	//se eliminan las soluciones dominadas
	for(int i=size-1;i>=0;i--)
	{
		//eliminacion desde el final del vector hasta el principio
		if(poblacion[i].dominated)
		{
			poblacion.erase(poblacion.begin()+i);
		}
	}
}

void Inmune::actualizar_dominancia(vector<Solution> &poblacion)
{
	int size = poblacion.size();
	//revisar si son dominados con respecto a otras soluciones
	for(int i=0;i<poblacion.size();i++)
	{
		poblacion[i].set_dominated(poblacion);
	}
}

void Inmune::seleccionar_mejores_anticuerpos()
{	
	//se elige un porcentaje de los mejores individuos
	int seleccion = (this->ss.solutions.size()*this->p_afinidad_clones + 0.5f);
	this->ss.solutions.resize(seleccion);
	
	/*
	this->clon.solutions.resize(0);
	this->clon.solutions.reserve(seleccion);
	
	for(unsigned int i=0;i<seleccion;i++){
		this->clon.solutions.push_back(this->ss.solutions[i]);
	}
	
	this->ss.solutions.resize(0);
	*/
}

int Inmune::cantidad_mutaciones(float &valor, float &menor_x, float &mayor_x, int &menor_y, int &mayor_y){
	//cout << "valor=" << valor << ", menor_x="<< menor_x << ", mayor_x=" << mayor_x << ", menor_y="<< menor_y << ", mayor_y=" << mayor_y << endl;
	float result = menor_y+((mayor_y-menor_y)/(mayor_x-menor_x))*(valor-menor_x);
	return (int)(result+0.5f);
}

void Inmune::clonar_anticuerpos()
{
	int random;
	this->pop_bc = this->ss.solutions.size();
	for(int i=0;i<this->clon_size-this->pop_bc;i++)
	{
		random = rand()%(this->pop_bc);
		//cout << "i " << i << " random " << random << endl;
		this->ss.solutions.push_back(this->ss.solutions[random]);
	}
	
}

void Inmune::mutar_clones(Problem &p, vector<RouteInfo> &routes_info, ShortestRoute* &sr){
	int** demand = p.get_demand();
	int** travel_times = p.get_travel_times();
	vector<BusStop> bus_stops = p.get_bus_stops();
	int size = p.get_size();
	float lbfo1 = p.get_lbfo1();
	float lbfo2 = p.get_lbfo2();
	//cout << "lbfo1 " << lbfo1 << ", lbfo2 " << lbfo2 << endl;
	int menor_mut = 1;
	float menor_q = this->ss.solutions[0].quality;
	float mayor_q = this->ss.solutions[this->pop_bc-1].quality;
	for(int i=0;i<this->ss.solutions.size();i++){
		//cout << "solution " << i << endl;
		//cout << "r " << ss.solutions[i].ranking_pareto << ": fo2 " << this->ss.solutions[i].fo2 << " fo1 " << this->ss.solutions[i].fo1 << " apt " << this->ss.solutions[i].quality << endl; 
		int tam_routes = this->ss.solutions[i].routes.size();
		//int cant = cantidad_mutaciones(this->ss.solutions[i].quality,this->ss.solutions[this->pop_bc-1].quality,this->ss.solutions[0].quality,menor_mut,tam_routes);
		int cant = cantidad_mutaciones(this->ss.solutions[i].quality,menor_q,mayor_q,menor_mut,tam_routes);

		//cout << "solution " << i << ", cant " << cant << endl;
		for(int j=0;j<cant;j++){
			if(this->ss.solutions[i].mutation(routes_info,travel_times,size,bus_stops)){
				//cout << "fo1 " << this->clon.solutions[i].fo1 << " fo2 " << this->clon.solutions[i].fo2 << " apt " << this->clon.solutions[i].quality << endl;
				this->ss.solutions[i].setFO1(*sr,demand);
				this->ss.solutions[i].setFO2(size,travel_times);
				this->ss.solutions[i].setQuality(this->alpha,this->beta,lbfo1,lbfo2);
				//cout << "fo1 " << this->clon.solutions[i].fo1 << " fo2 " << this->clon.solutions[i].fo2 << " apt " << this->clon.solutions[i].quality << endl;
			}
			else{
				//cout << i << " false" << endl;
			}		
		}
		//cout << "r " << ss.solutions[i].ranking_pareto << ": fo2 " << this->ss.solutions[i].fo2 << " fo1 " << this->ss.solutions[i].fo1 << " apt " << this->ss.solutions[i].quality << endl; 
		
	}
	
	
}

void Inmune::clones_a_poblacion(){
	
	sort(this->ss.solutions.begin(), this->ss.solutions.end());
	this->ss.solutions.resize(this->pop_size);
	//cout << "Clones a poblacion" << endl;
	//cout << "Poblacion size " << this->ss.solutions.size() <<  endl;
	/*int cantidad = 0;
	if(this->clon.solutions.size() > 100){
		cantidad = 100;
	}
	else{
		cantidad = this->clon.solutions.size();
	}
	
	for(unsigned int i=0;i<cantidad;i++){
		this->ss.solutions.push_back(this->clon.solutions[i]);
	}
	this->clon.solutions.resize(0);
	*/
}

void Inmune::guardar_mejores_clones(){
	//cout << "Guardar mejores clones" << endl;
	
	int seleccion = (this->pop_size*this->p_clones + 0.5f);
	for(unsigned int i=0;i<seleccion;i++){
		this->memory.solutions.push_back(this->ss.solutions[i]);
	}
}

void Inmune::reemplazar_peores_soluciones(Problem &p, vector<RouteInfo> &ri, ShortestRoute &sr){
	//cout << "Reemplazar peores soluciones" << endl;
	//cout << "obtener mejores afinidades de poblacion" << endl;
	vector<Solution>::iterator it;
	int seleccion = (this->pop_size*this->p_reemplazo + 0.5f);
	
	//delete worst solutions until solution set have pop_size-seleccion
	for(unsigned int i=0;i<this->ss.solutions.size()-seleccion;i++){
		this->ss.solutions.pop_back();
	}
	
	//add new solutions
	
	bool sol_conexa = false;
	for(unsigned int i=0;i<seleccion;i++){
		do{
			Solution *s = new Solution(p,ri,sr,this->alpha,this->beta);
			sol_conexa = s->check_connectivity(p.get_size()) && s->check_feasability();
			//cout << "sol conexa " << sol_conexa << endl;
			if(sol_conexa){
				if(this->ss.solutions.size()==0){
					this->ss.solutions.push_back(*s);
				}else{
					for(it=this->ss.solutions.begin(); it!=this->ss.solutions.end(); it++){
						if(s->quality < it->quality){
							break;
						}
					}
					this->ss.solutions.insert(it,*s);	
				}
			}
			delete s;
		}while(!sol_conexa);
	}
}

void Inmune::cambiar_alpha_beta(){
	int random_number = 100*rand()%11;
	if(random_number<0){
		random_number = random_number*-1;
	}
	int a = random_number*10;
	int b = 100-random_number*10;
	this->alpha = a/100.0;
	this->beta = b/100.0;
	cout << "cambio, alpha=" << a << " beta=" << b << endl;
}
