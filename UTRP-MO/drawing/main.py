import pygame
import sys
from pygame.locals import *

def menos_uno(x):
	return int(x)-1

def generar_ruta(camino,vertices):
	cam = []
	for i in range(len(camino)):
		cam.append((a*vertices[i][0],ancho-a*vertices[i][1]))
	return cam

def dibujar_mapa():
	for i,j in edges:
		pygame.draw.lines(screen, black, False, [(a*vertex[i][0],ancho-a*vertex[i][1]),(a*vertex[j][0],ancho-a*vertex[j][1])], 1)
		
	for x,y in vertex.values():
		pygame.draw.circle(screen, black, (int(a*x),int(ancho-a*y)), 10, 0)
	

def actualizar_ruta(sol,ruta,vertex):
	p = generar_ruta(d_rutas[sol]["rutas"][ruta],vertex)
	print p
	pygame.draw.lines(screen, red, False, p, 1)
	for i in p:
		pygame.draw.circle(screen, red, (int(a*i[0]),int(ancho-a*i[1])), 10, 0)
	
prefix = "Mumford0"
largo = 800
ancho = 600
n_rutas = 12
results = "rutas.txt"
#factor de zoom
a=34

arch = open(prefix+"Coords.txt")
vertex = {}
i=0
for l in arch:
	l = l.strip().split()
	if len(l)==2:
		vertex[i]=tuple(map(float,l))
		i+=1

arch.close()


arch = open(prefix+"TravelTimes.txt")
edges = []
i=0
for l in arch:
	l = l.strip().split()

	if len(l)>0:		
		for j in range(i,len(vertex)):
			if l[j] != "Inf":
				edges.append((i,j))	
		i+=1
arch.close()

arch = open(results)
cont_lin = 0
d_rutas = {}
r = -1
for l in arch:
	if cont_lin%(n_rutas+1) == 0:
		r+=1
		d_rutas[r] = {}
		d_rutas[r]["rutas"] = []
		
		l = l.strip().split()
		d_rutas[r]["info"] = " ".join(l)
	else:
		l = l.strip().split(", ")
		d_rutas[r]["rutas"].append(map(menos_uno,l))
	cont_lin+=1
arch.close()

print d_rutas

red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
darkBlue = (0,0,128)
white = (255,255,255)
black = (0,0,0)
pink = (255,200,200)

pygame.init()

screen = pygame.display.set_mode((largo,ancho))

pygame.display.set_caption("IAA Viewer v0.1 - "+prefix)

screen.fill(white)
dibujar_mapa()	
pygame.display.update()
while True:	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			sys.exit()	
		elif event.type == KEYDOWN and event.key == K_UP:
			screen.fill(white)
			pygame.display.update()
		elif event.type == KEYDOWN and event.key == K_DOWN:
			actualizar_ruta(0,1,vertex)
			pygame.display.update()
		elif event.type == KEYDOWN and event.key == K_LEFT:
			actualizar_ruta(0,2,vertex)
			pygame.display.update()
		elif event.type == KEYDOWN and event.key == K_RIGHT:
			dibujar_mapa()
			pygame.display.update()



